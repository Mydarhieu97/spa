const switchView = async (viewName) => {
    const viewNamePath = `../pages/${viewName}.html`;
    let res = await fetch(viewNamePath).then(data => data.text());
    const el = document.getElementById("main-content");
    el.innerHTML = res;
}

const setColorMode = () => {
    const currentWidth = window.innerWidth;
    const currentTime = new Date().getHours();
    const sideBar = document.getElementById("side-bar-menu");
    if(currentTime > 18 || currentTime < 6) {
        sideBar.classList.add("dark-bg");
        document.getElementById("dark-bg").classList.add("mode-selected");
        document.body.setAttribute('style', 'background-color: black');
    } else {
        sideBar.classList.add("light-bg");
        document.getElementById("light-bg").classList.add("mode-selected");
        document.body.setAttribute('style', 'background-color: white');
    }

    if(currentWidth < 480) {
        sideBar.classList.remove("open");
        sideBar.classList.add("close");
    }
}

const randomBackground = () => {
    var rn = Math.floor((Math.random() * 150) + 100);
    var rs = Math.floor((Math.random() * 11) + 4);
    var t = new Trianglify({
    x_gradient: Trianglify.colorbrewer.Spectral[rs],
        noiseIntensity: 0,
        cellsize: rn
    });
    return t.generate(window.innerWidth, window.innerWidth+200);
}

document.addEventListener("DOMContentLoaded", () => {
    setColorMode();
    switchView("dash-board");
})

const toggleSidebar = () => {
    const sideBar = document.getElementById("side-bar-menu");
    sideBar.classList.toggle("open");
    sideBar.classList.toggle("close");
}

const changeColorMode = (event) => {
    document.querySelector(".sidebar-mode.mode-selected").classList.remove("mode-selected");
    const target = event.currentTarget;
    target.classList.add("mode-selected");
    const id = target.id;
    const sideBar = document.getElementById("side-bar-menu");
    sideBar.classList.remove("blurry-ui", "light-bg", "dark-bg");
    sideBar.classList.add(id);
    if(id === "blurry-ui") {
        const pattern = randomBackground();
        document.body.setAttribute('style', 'background-image: ' + pattern.dataUrl);
    }
    if(id === "dark-bg") {
        document.body.setAttribute('style', 'background-color: black');
    }
    if(id === "light-bg") {
        document.body.setAttribute('style', 'background-color: white');
    }
}

const selectTab = (event) => {
    document.querySelector(".sidebar-item.active").classList.remove("active");
    event.currentTarget.classList.add("active");
}

const openTheSubMenu = (id) => {
    const expandedItem = document.querySelector(".sidebar-main-item.expanded");
    expandedItem ? expandedItem.classList.remove("expanded") : () => {};
    const target = document.getElementById(id);
    const mainItem = target.querySelector(".sidebar-main-item");
    mainItem.classList.add("expanded");
}
